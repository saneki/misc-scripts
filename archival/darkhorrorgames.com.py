#!/usr/bin/env python

from collections import namedtuple
from pyquery import PyQuery as pq
import re
import requests
import shutil
from urllib.parse import urlparse
from urllib.request import urlopen

# Link to all horror games (sorted A-Z), given page number
URL_FORMAT = 'http://www.darkhorrorgames.com/all-games/nameasc/{}/'

GAMES = 'http://www.darkhorrorgames.com/games/'
GAME_PATTERN = GAMES + r'([a-zA-Z0-9_-]+\.(swf|unity3d))'

# Proper CSS path to links
# .generalgames_box .category_container ul li a:href
# Shortcut: .generalgames_box a.tooltip

GameFileInfo = namedtuple('GameFileInfo', ('name', 'ext'))
GameInfo = namedtuple('GameInfo', ('title', 'fileinfo', 'iframe'))

def parse_all_games_pages(pages=range(10), download=True):
    """ Parse all games on each page in the given range. """
    for page in pages:
        urls = parse_games_page(page)
        for url in urls:
            parsed = urlparse(url)
            if is_playable_url(parsed):
                r = requests.get(url)
                game = parse_game(r)
                if game.fileinfo is not None:
                    full = GAMES + game.fileinfo.name
                    print('Downloading: {}'.format(full))
                    if download:
                        download_to(full, game.fileinfo.name)
                elif game.iframe is not None:
                    print('Iframe source ({0}): {1}'.format(game.title, game.iframe))
                else:
                    print('No file found: {}'.format(url))
            else:
                print('Not a game: {}'.format(url))

def is_playable_url(parsed_url):
    """ Whether or not a game URL appears to be a playable game. """
    return parsed_url.path.startswith('/play/')

def parse_games_page(page, url_format=URL_FORMAT):
    """ Parse a games listing page. """
    url = url_format.format(page)
    r = requests.get(url)
    d = pq(r.text)
    box = d('.generalgames_box')
    anchors = box.find('a.tooltip')
    return tuple(pq(a).attr('href') for a in anchors)

def parse_game(res):
    """ Parse a game page given a response. """
    title = parse_game_title(res)
    fileinfo = parse_game_file(res)
    iframe = parse_game_iframe(res)
    return GameInfo(title=title, fileinfo=fileinfo, iframe=iframe)

def parse_game_title(res):
    """ Try and parse the game title. """
    d = pq(res.text)
    content = d('.maincontent')
    h1 = content.eq(0).find('h1')
    return pq(h1).text()

def parse_game_file(res, pattern=GAME_PATTERN):
    """ Try and parse the game's file name and extension, if any. """
    result = re.search(pattern, res.text)
    if result is not None:
        name, ext = result.group(1, 2)
        return GameFileInfo(name=name, ext=ext)

def parse_game_iframe(res):
    """ Try and parse the game's iframe source, if any. """
    d = pq(res.text)
    container = d('#ava-game_container')
    children = container.find('iframe')
    for iframe in children:
        return pq(iframe).attr('src')

def download_to(url, file_name):
    """ Download the contents at a URL to a file. """
    # https://stackoverflow.com/a/7244263/8489921
    with urlopen(url) as response, open(file_name, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

def main(args):
    """ Main function. """
    pages = range(args.page_start, args.page_end + 1)
    parse_all_games_pages(pages=pages, download=args.download)

if __name__ == '__main__':
    import argparse
    def get_parser():
        parser = argparse.ArgumentParser()
        parser.add_argument('-e', '--page-end', type=int, default=9, help='Last page to search')
        parser.add_argument('-s', '--page-start', type=int, default=1, help='First page to search')
        parser.add_argument('-z', '--no-download', dest='download', action='store_false', help="Pretend to download but don't")
        return parser

    args = get_parser().parse_args()
    main(args)
